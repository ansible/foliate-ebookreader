# Foliate ebook reader

Foliate works well with Calibre Web via OPDS.


## Foliate via PPA: broken catalogs (investigating)

https://github.com/johnfactotum/foliate/issues/918

Hm, why does a firejail profile exist for Foliate?
That is, why does this file exist: `/etc/firejail/com.github.johnfactotum.Foliate.profile`?

Let's uninstall firejail.
```
taha@asks2:~
$ sudo apt purge firejail firejail-profiles
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following packages were automatically installed and are no longer required:
  python3-cpuinfo python3-dns python3-ifaddr python3-kerberos python3-lz4 python3-lzo python3-opengl python3-pyinotify
  python3-rencode python3-uritools python3-xdg python3-zeroconf xpra xserver-xorg-video-dummy
Use 'sudo apt autoremove' to remove them.
The following packages will be REMOVED:
  firejail* firejail-profiles*
0 upgraded, 0 newly installed, 2 to remove and 31 not upgraded.
After this operation, 3,282 kB disk space will be freed.
Do you want to continue? [Y/n]
(Reading database ... 256272 files and directories currently installed.)
Removing firejail-profiles (0.9.70-1~0ubuntu22.04.0) ...
Removing firejail (0.9.70-1~0ubuntu22.04.0) ...
Processing triggers for man-db (2.10.2-1) ...
(Reading database ... 256195 files and directories currently installed.)
Purging configuration files for firejail (0.9.70-1~0ubuntu22.04.0) ...
Purging configuration files for firejail-profiles (0.9.70-1~0ubuntu22.04.0) ...
```

Seems like this removed the firejail profile we saw above.

Removed Foliate flatpak and reinstalled Foliate from PPA.
```
** (process:2): WARNING **: 00:17:52.661: Error writing credentials to socket: Error sending message: Broken pipe
CONSOLE SECURITY ERROR Origin null is not allowed by Access-Control-Allow-Origin. Status code: 200
CONSOLE JS ERROR Fetch API cannot load https://catalog.feedbooks.com/publicdomain/browse/awards.atom?lang=en due to access control checks.

(com.github.johnfactotum.Foliate:246502): Gjs-WARNING **: 00:17:53.415: JS ERROR: Error: TypeError: Load failed
_handleAction@resource:///com/github/johnfactotum/Foliate/js/opds.js:485:50
OpdsClient/<@resource:///com/github/johnfactotum/Foliate/js/opds.js:470:18
main@resource:///com/github/johnfactotum/Foliate/js/main.js:478:24
run@resource:///org/gnome/gjs/modules/script/package.js:206:19
@/usr/bin/com.github.johnfactotum.Foliate:9:17

CONSOLE SECURITY ERROR Cross-origin redirection to https://standardebooks.org/feeds/opds/all denied by Cross-Origin Resource Sharing policy: Origin null is not allowed by Access-Control-Allow-Origin. Status code: 301
CONSOLE JS ERROR Fetch API cannot load https://standardebooks.org/opds/all due to access control checks.

(com.github.johnfactotum.Foliate:246502): Gjs-WARNING **: 00:17:53.605: JS ERROR: Error: TypeError: Load failed
_handleAction@resource:///com/github/johnfactotum/Foliate/js/opds.js:485:50
OpdsClient/<@resource:///com/github/johnfactotum/Foliate/js/opds.js:470:18
main@resource:///com/github/johnfactotum/Foliate/js/main.js:478:24
run@resource:///org/gnome/gjs/modules/script/package.js:206:19
@/usr/bin/com.github.johnfactotum.Foliate:9:17
```

Socket? Related to X11? Something that i3wm does not provide?
https://github.com/johnfactotum/foliate/issues/594
Could it be related to Nvidia graphics drivers version? Should we update them?

https://github.com/johnfactotum/foliate/commit/bc2c3023884e12a2688ee7e2d4a90890d3bc36f9
It seems Foliate relies on JS Webkit2 to connect to the "Secret service" on the system.
https://github.com/johnfactotum/foliate/pull/569

I have `seahorse` installed ("passwords and keys", GUI gnupg frontend).
Looking at

```
### on ubuntu jammy
# seahorse v41.0-2

$ gpg --version
gpg (GnuPG) 2.2.27
libgcrypt 1.9.4
Copyright (C) 2021 Free Software Foundation, Inc.
License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: ~/.gnupg
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2
```


```
### on ubuntu bionic
# seahorse v3.20.0-5

$ gpg --version
gpg (GnuPG) 2.2.4
libgcrypt 1.8.9
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: ~/.gnupg
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2
```

Let's make sure we have nuked all config and cache directories:
```
rm -rf ~/.var/app/com.github.johnfactotum.Foliate/
rm -rf ~/.local/share/flatpak/repo/refs/heads/deploy/app/com.github.johnfactotum.Foliate/
rm -rf ~/.cache/com.github.johnfactotum.Foliate
rm -rf ~/.local/share/flatpak/repo/refs/remotes/flathub/app/com.github.johnfactotum.Foliate
rm -rf ~/.local/share/flatpak/repo/refs/remotes/flathub/app/com.github.johnfactotum.Foliate/x86_64
rm -rf ~/.local/share/flatpak/repo/refs/remotes/flathub/runtime/com.github.johnfactotum.Foliate.Locale
```

Alright, at this point, `plocate` shows the following:
```
taha@asks2:~
$ locate Foliate
/usr/bin/com.github.johnfactotum.Foliate
/usr/share/com.github.johnfactotum.Foliate
/usr/share/applications/com.github.johnfactotum.Foliate.desktop
/usr/share/com.github.johnfactotum.Foliate/assets
/usr/share/com.github.johnfactotum.Foliate/com.github.johnfactotum.Foliate.data.gresource
/usr/share/com.github.johnfactotum.Foliate/com.github.johnfactotum.Foliate.src.gresource
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack
/usr/share/com.github.johnfactotum.Foliate/assets/client.html
/usr/share/com.github.johnfactotum.Foliate/assets/epub-viewer-cb.html
/usr/share/com.github.johnfactotum.Foliate/assets/epub-viewer-nocsp.html
/usr/share/com.github.johnfactotum.Foliate/assets/epub-viewer.css
/usr/share/com.github.johnfactotum.Foliate/assets/epub-viewer.html
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/__init__.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/compatibility_utils.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/kindleunpack.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_cover.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_dict.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_header.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_html.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_index.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_k8proc.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_k8resc.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_nav.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_ncx.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_opf.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_pagemap.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_sectioner.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_split.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_uncompress.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobi_utils.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/mobiml2xhtml.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/unipath.py
/usr/share/com.github.johnfactotum.Foliate/assets/KindleUnpack/unpack_structure.py
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/main.js
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/README.md
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/dist
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/package.json
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/dist/wasm-gen
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/dist/worker-bundle.js
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/dist/wasm-gen/libarchive.js
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/dist/wasm-gen/libarchive.wasm
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src/compressed-file.js
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src/libarchive.js
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src/webworker
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src/webworker/archive-reader.js
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src/webworker/wasm-gen
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src/webworker/wasm-module.js
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src/webworker/worker.js
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src/webworker/wasm-gen/libarchive.js
/usr/share/com.github.johnfactotum.Foliate/assets/libarchivejs/libarchivejs-1.3.0/src/webworker/wasm-gen/libarchive.wasm
/usr/share/glib-2.0/schemas/com.github.johnfactotum.Foliate.gschema.xml
/usr/share/icons/hicolor/scalable/apps/com.github.johnfactotum.Foliate.svg
/usr/share/icons/hicolor/symbolic/apps/com.github.johnfactotum.Foliate-symbolic.svg
/usr/share/locale/cs/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/de/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/es/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/eu/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/fr/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/id/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/ie/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/it/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/ko/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/nb/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/nl/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/nn/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/pt_BR/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/ru/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/sv/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/uk/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/zh_CN/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/locale/zh_TW/LC_MESSAGES/com.github.johnfactotum.Foliate.mo
/usr/share/metainfo/com.github.johnfactotum.Foliate.metainfo.xml
```

Restarted Foliate. Same error.
```
taha@asks2:~
$ /usr/bin/com.github.johnfactotum.Foliate
CONSOLE SECURITY ERROR Origin null is not allowed by Access-Control-Allow-Origin. Status code: 200
CONSOLE JS ERROR Fetch API cannot load https://catalog.feedbooks.com/publicdomain/browse/awards.atom?lang=en due to access control checks.

(com.github.johnfactotum.Foliate:248912): Gjs-WARNING **: 00:46:22.855: JS ERROR: Error: TypeError: Load failed
_handleAction@resource:///com/github/johnfactotum/Foliate/js/opds.js:485:50
OpdsClient/<@resource:///com/github/johnfactotum/Foliate/js/opds.js:470:18
main@resource:///com/github/johnfactotum/Foliate/js/main.js:478:24
run@resource:///org/gnome/gjs/modules/script/package.js:206:19
@/usr/bin/com.github.johnfactotum.Foliate:9:17

CONSOLE SECURITY ERROR Cross-origin redirection to https://standardebooks.org/feeds/opds/all denied by Cross-Origin Resource Sharing policy: Origin null is not allowed by Access-Control-Allow-Origin. Status code: 301
CONSOLE JS ERROR Fetch API cannot load https://standardebooks.org/opds/all due to access control checks.

(com.github.johnfactotum.Foliate:248912): Gjs-WARNING **: 00:46:22.909: JS ERROR: Error: TypeError: Load failed
_handleAction@resource:///com/github/johnfactotum/Foliate/js/opds.js:485:50
OpdsClient/<@resource:///com/github/johnfactotum/Foliate/js/opds.js:470:18
main@resource:///com/github/johnfactotum/Foliate/js/main.js:478:24
run@resource:///org/gnome/gjs/modules/script/package.js:206:19
@/usr/bin/com.github.johnfactotum.Foliate:9:17

** (process:2): WARNING **: 00:46:23.306: Error writing credentials to socket: Error sending message: Broken pipe
```

Do we have runtime dependencies (as far as I can make them out)?
https://github.com/johnfactotum/foliate/issues/766#issuecomment-897603165
```
webkit2gtk v2.36.3-0ubuntu0.22.04.1 # Web content engine library for GTK
gjs v1.72.0-3 # Mozilla-based javascript bindings for the GNOME platform
```

More pointers towards Nvidia driver...
https://github.com/johnfactotum/foliate/issues/707
https://github.com/johnfactotum/foliate/issues/322
https://github.com/johnfactotum/foliate/issues/504

Took the leap. Upgraded from nvidia 470 to 510.
Subsequent `apt update` also upgraded some other, possible related, packages:
```
$ sudo apt full-upgrade
The following NEW packages will be installed:
  libimath29 liblerc-dev liblerc3 libopenexr30
The following packages will be upgraded:
  brave-browser firefox-esr gir1.2-harfbuzz-0.0 gir1.2-javascriptcoregtk-4.0 gir1.2-webkit2-4.0 google-chrome-stable libde265-0
  libexiv2-27 libfreetype-dev libfreetype6 libfreetype6-dev libharfbuzz-dev libharfbuzz-gobject0 libharfbuzz-icu0 libharfbuzz0b
  libjavascriptcoregtk-4.0-18 libopencv-core4.5d libopencv-imgcodecs4.5d libopencv-imgproc4.5d libopencv-videoio4.5d libtiff-dev
  libtiff-tools libtiff5 libtiffxx5 libwebkit2gtk-4.0-37 opera-stable python3-jwt qownnotes signal-desktop tailscale
```

Getting weird "file not found" errors on starting Foliate, plus the same old
error messages as before.

Decided to uninstall completely and remove all traces.
```
sudo apt purge foliate
rm -rf ~/.cache/com.github.johnfactotum.Foliate
rm -rf ~/.local/share/com.github.johnfactotum.Foliate
```

Let's reinstall:
```
taha@asks2:~
$ sudo apt install foliate 
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  gir1.2-gspell-1 gir1.2-handy-0.0 gjs libgjs0g libhandy-0.0-0 libmozjs-91-0
Suggested packages:
  gir1.2-tracker-3.0 espeak festival sdcv dictd dictd-dictionary hyphen-af
  hyphen-as hyphen-bg hyphen-bn hyphen-ca hyphen-cs hyphen-da hyphen-de
  hyphen-el hyphen-en-ca hyphen-en-gb hyphen-en-us hyphen-es hyphen-fi hyphen-fr
  hyphen-ga hyphen-gl hyphen-gu hyphen-hi hyphen-hr hyphen-hu hyphen-id
  hyphen-is hyphen-it hyphen-kn hyphen-lt hyphen-lv hyphen-ml hyphen-mr
  hyphen-nl hyphen-no hyphen-or hyphen-pa hyphen-pl hyphen-pt-br hyphen-pt-pt
  hyphen-ro hyphen-ru hyphen-show hyphen-sk hyphen-sl hyphen-sr hyphen-sv
  hyphen-ta hyphen-te hyphen-uk hyphen-zu
The following NEW packages will be installed:
  foliate gir1.2-gspell-1 gir1.2-handy-0.0 gjs libgjs0g libhandy-0.0-0 libmozjs-91-0
0 upgraded, 7 newly installed, 0 to remove and 2 not upgraded.
Need to get 0 B/5,713 kB of archives.
After this operation, 19.9 MB of additional disk space will be used.
```

Same error. Plus it seems the "file not found" error is because Foliate is now
for some reason trying to open the debug log file I redirected the command to
earlier. I'm frankly baffled and don't understand how the program has any memory
of that across a reboot. Anyway, Flatpak it is.


## Foliate via Flatpak: fails to retain Calibre credentials

https://github.com/johnfactotum/foliate/issues/877
Otherwise appears to work OK.

Let's start it from the terminal, to observe stdout:
```
$ flatpak run -v com.github.johnfactotum.Foliate
F: No installations directory in /etc/flatpak/installations.d. Skipping
F: Opening system flatpak installation at path /var/lib/flatpak
F: Opening user flatpak installation at path ~/.local/share/flatpak
F: Opening user flatpak installation at path ~/.local/share/flatpak
F: Skipping parental controls check for app/com.github.johnfactotum.Foliate/x86_64/stable since parental controls are disabled globally
F: Opening user flatpak installation at path ~/.local/share/flatpak
F: ~/.local/share/flatpak/runtime/org.gnome.Platform/x86_64/42/472b0f1f1b9d70b8e146872471c025bd1f8f04159ac48f36c0d56fbbc47f0a2e/files/lib32 does not exist
F: Cleaning up unused container id 2804035515
F: Cleaning up per-app-ID state for com.github.johnfactotum.Foliate
F: Cleaning up unused container id 2246310641
F: Cleaning up per-app-ID state for com.github.johnfactotum.Foliate
F: Cleaning up unused container id 4074144401
F: Cleaning up per-app-ID state for com.github.johnfactotum.Foliate
F: Cleaning up unused container id 714190434
F: Cleaning up per-app-ID state for com.github.johnfactotum.Foliate
F: Cleaning up unused container id 1957932517
F: Cleaning up per-app-ID state for com.github.johnfactotum.Foliate
F: Cleaning up unused container id 4072146261
F: Cleaning up per-app-ID state for com.github.johnfactotum.Foliate
F: Cleaning up unused container id 2242591553
F: Cleaning up per-app-ID state for com.github.johnfactotum.Foliate
F: Cleaning up unused container id 1986342852
F: Cleaning up per-app-ID state for com.github.johnfactotum.Foliate
F: Allocated instance id 4077311267
F: Add values in dir '/com/github/johnfactotum/Foliate/', prefix is '/com/github/johnfactotum/Foliate/'
F: Add defaults in dir /com/github/johnfactotum/Foliate/
F: Add locks in dir /com/github/johnfactotum/Foliate/
F: writing D-Conf values to ~/.var/app/com.github.johnfactotum.Foliate/config/glib-2.0/settings/keyfile
F: Allowing dri access
F: Allowing wayland access
CONSOLE SECURITY ERROR The Content Security Policy directive 'sandbox' is ignored when delivered via an HTML meta element.
```

Anyway, before adding my Calibre library to Foliate, I *deleted* the existing
password in my secret store (using `seahorse`).
Well, after restarting Foliate, no new password entries are created.
And Foliate does not remember our Calibre password. And nothing related in the log.

+ https://github.com/johnfactotum/foliate/issues/877
+ https://wiki.archlinux.org/title/GNOME/Keyring


## Calibre discovery feed URL

How to make the Catalog listing in Foliate display
a list of the most recent books or similar (right now it only displays
the main categories, such as Series, Ratings, Random, Unread, etc.)
This seems to be a matter of figuring out the OPDS feed URL format...

| URL   | Result |
| ---   | ---    |
| /opds | lists categories, including hot, disccover, and more |
| /opds/hot | lists popular books, with graphical bookcovers |
| /opds/discover | lists random books, with graphical bookcovers |
| /search.opds/?sort_order=random | error "unable to load OPDS feed" |
| /opds/recent | error "unable to load OPDS feed" |



## Local library/catalogs directory (for unison sync)

Foliate installed via PPA stores its local files in
`~/.local/share/com.github.johnfactotum.Foliate`:

```
~/.local/share/com.github.johnfactotum.Foliate
$ tree
.
├── books
│   └── Calibans%20War.epub
├── catalogs
│   └── catalogs.json
├── e64513f2-266d-4b8a-b1ba-437597ee86c4.json
└── library
    └── uri-store.json
```

Could be useful to sync this directory between my workstations.


Folite installed via Flatpak stores its local library/catalogs in
`~/.var/app/com.github.johnfactotum.Foliate/data/com.github.johnfactotum.Foliate/`:
```
$ tree
~/.var/app/com.github.johnfactotum.Foliate/data/com.github.johnfactotum.Foliate/
├── 9780735211629.json
├── a9781786634849.json
├── books
│   ├── urn%3Auuid%3A878cb1cb-94e7-44bd-8754-a56de128c27f@2022-01-17T14%3A58%3A47%2B00%3A00@Enemy%20of%20All.epub
│   └── urn%3Auuid%3A988d7744-d9b9-461d-857f-8eb91393d1da@2021-04-26T20%3A56%3A42%2B00%3A00@Sinews%20of%20War.epub
├── catalogs
│   └── catalogs.json
└── library
    └── uri-store.json
```


## Refs

+ https://github.com/johnfactotum/foliate/issues/451
+ https://groups.google.com/g/openpub
